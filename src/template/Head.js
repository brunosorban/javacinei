import React from 'react';

function Head() {
    return (
        <header id='header'>
        <div id='header-title'>
            <img src="https://i.imgur.com/6PHyWdC.png" alt="logo"/>
            <h1 id='title'>Vacinei Dashboard</h1>
        </div>
    </header>
    );

}
export default Head;